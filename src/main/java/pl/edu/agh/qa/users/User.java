package pl.edu.agh.qa.users;

import pl.edu.agh.qa.items.Item;

import java.util.ArrayList;
import java.util.List;

public class User {
    private String firstName;
    private String lastName;
    private int cardNumber;
    private List<Item> rentedItems;

    public User() {
    }

    public User(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;

        rentedItems = new ArrayList<Item>();
    }

    public void print() {
        String str = firstName + ";" + lastName + ";" + cardNumber + ";";

        if (this instanceof Student) {
            str += "S";
        } else str += "L";

        System.out.println(str);
    }

    public void setCardNumber(int cardNumber) {
        this.cardNumber = cardNumber;
    }

    public int getCardNumber() {
        return cardNumber;
    }

    public List<Item> getRentedItems() {
        return rentedItems;
    }
}

