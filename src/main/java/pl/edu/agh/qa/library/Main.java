package pl.edu.agh.qa.library;


import pl.edu.agh.qa.items.Book;
import pl.edu.agh.qa.items.Magazine;
import pl.edu.agh.qa.users.Lecturer;
import pl.edu.agh.qa.users.Student;

public class Main {

    public static void main(String[] args) {
        Library lib = new Library();

        lib.addUserToLibrary(new Student("Kasia", "Kowalska"));
        lib.addUserToLibrary(new Student("Adam", "Sliwka"), new Lecturer("Krzysztof", "Lewandowski"));
        lib.addUserToLibrary(new Lecturer("Marek", "Malendowicz"));
        lib.addUserToLibrary(new Lecturer("Kamil", "Król"), new Student("Adrian", "Warzecha"));
        lib.addUserToLibrary(new Student("Arkadiusz", "Milik"));

//        lib.printListOfUsers();

        lib.addItemToLibrary(new Book("Adam Mickiewicz", "Dziady"));
        lib.addItemToLibrary(new Magazine("01/2020", "Focus"));
        lib.addItemToLibrary(new Book("Adam Mickiewicz", "Dziady"));
        lib.addItemToLibrary(new Magazine("01/2020", "Focus"));
        lib.addItemToLibrary(new Book("Adam Mickiewicz", "Dziady"));
        lib.addItemToLibrary(new Magazine("01/2020", "Focus"));
        lib.addItemToLibrary(new Book("Adam Mickiewicz", "Dziady"));
        lib.addItemToLibrary(new Magazine("01/2020", "Focus"));
        lib.addItemToLibrary(new Book("Adam Mickiewicz", "Dziady"));
        lib.addItemToLibrary(new Magazine("01/2020", "Focus"));
        lib.addItemToLibrary(new Magazine("04/2020", "Discovery Magazine"));

        lib.importItemsFromFile("plik_wejsciowy.csv");


//        lib.printListOfBooks();
//        lib.printListOfMagazines();

//        for (int i = 0; i < 3; i++)
//            lib.rentItemToUser(lib.getListOfItems().get(0), lib.getListOfUsers().get(0));
//
//        for (int i = 0; i < 3; i++)
//            lib.rentItemToUser(lib.getListOfItems().get(1), lib.getListOfUsers().get(0));
//
//
//        for (int i = 0; i < 3; i++)
//            lib.rentItemToUser(lib.getListOfItems().get(0), lib.getListOfUsers().get(1));
//            lib.rentItemToUser(lib.getListOfItems().get(1), lib.getListOfUsers().get(1));
//
//        for (int i = 0; i < 6; i++)
//            lib.rentItemToUser(lib.getListOfItems().get(0), lib.getListOfUsers().get(2));
//
//        for (int i = 0; i < 5; i++)
//            lib.rentItemToUser(lib.getListOfItems().get(1), lib.getListOfUsers().get(2));
//
//
//        lib.rentItemToUser(lib.getListOfItems().get(2), lib.getListOfUsers().get(3));


        lib.rentItemToUser(lib.getListOfItems().get(0), lib.getListOfUsers().get(0));
        lib.rentItemToUser(lib.getListOfItems().get(1), lib.getListOfUsers().get(1));
        lib.rentItemToUser(lib.getListOfItems().get(2), lib.getListOfUsers().get(2));
        lib.rentItemToUser(lib.getListOfItems().get(3), lib.getListOfUsers().get(3));
        lib.rentItemToUser(lib.getListOfItems().get(4), lib.getListOfUsers().get(4));
        lib.rentItemToUser(lib.getListOfItems().get(5), lib.getListOfUsers().get(5));
        lib.rentItemToUser(lib.getListOfItems().get(6), lib.getListOfUsers().get(6));

        lib.printListOfUsers();
        lib.printListOfBooks();
        lib.printListOfMagazines();

        lib.exportUsersWithItemsToFile("plik_wyjsciowy.csv");

    }
}
