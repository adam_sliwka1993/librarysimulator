package pl.edu.agh.qa.library;


import pl.edu.agh.qa.items.Book;
import pl.edu.agh.qa.items.Item;
import pl.edu.agh.qa.items.Magazine;
import pl.edu.agh.qa.users.Lecturer;
import pl.edu.agh.qa.users.Student;
import pl.edu.agh.qa.users.User;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class Library {
    private ArrayList<Item> listOfItems;
    private ArrayList<User> listOfUsers;
    private static int nextCardNumber;

    public Library() {
        listOfItems = new ArrayList<Item>();
        listOfUsers = new ArrayList<User>();
        this.nextCardNumber = 0;
    }

    public void addUserToLibrary(User... users) {
        for (User u : users) {
            listOfUsers.add(u);
            u.setCardNumber(nextCardNumber);
            nextCardNumber++;
        }
    }

    public void printListOfUsers() {
        for (User user : listOfUsers) {
            user.print();
        }
    }

    public void addItemToLibrary(Item... item) {
        for (Item i : item) {
            boolean add = true;
            for (Item i2 : listOfItems) {
                if (i2 instanceof Book && i instanceof Book) {
                    if (((Book) i2).getAuthor().equals(((Book) i).getAuthor()) && i2.getTitle().equals(i.getTitle())) {
                        i2.numberOfAll += i.numberOfAll;
                        i2.numberOfAvailable += i.numberOfAll;
                        add = false;
                    }
                } else if (i2 instanceof Magazine && i instanceof Magazine) {
                    if (i2.getTitle().equals(i.getTitle()) && ((Magazine) i2).getNumber().equals(((Magazine) i).getNumber())) {
                        i2.numberOfAll += i.numberOfAll;
                        i2.numberOfAvailable += i.numberOfAll;
                        add = false;
                    }
                }
            }
            if (add) {
                listOfItems.add(i);
            }
        }
    }

    public boolean rentItemToUser(Item item, User user) {
        if (item.numberOfAvailable == 0) return false;
        int count = user.getRentedItems().size();
        if (user instanceof Student && count >= 4) return false;
        if (user instanceof Lecturer && count >= 10) return false;
        user.getRentedItems().add(item);
        item.numberOfAvailable--;
        return true;
    }

    public void printListOfBooks() {
        for (Item item : listOfItems) {
            if (item instanceof Book) {
                item.print();
            }
        }
    }

    public void printListOfMagazines() {
        for (Item item : listOfItems) {
            if (item instanceof Magazine) {
                item.print();
            }
        }
    }

    public void importItemsFromFile(String csvFile) {
        String row;
        try {
            BufferedReader csvReader = new BufferedReader(new FileReader(csvFile));
            while (true) {
                if (!((row = csvReader.readLine()) != null)) break;
                String[] data = row.split(";");
                if (data[3].equals("B")) {
                    Book book = new Book(data[1], data[0]);
                    book.numberOfAvailable = Integer.valueOf(data[2]);
                    book.numberOfAll = Integer.valueOf(data[2]);
                    addItemToLibrary(book);
                } else {
                    Magazine magazine = new Magazine(data[1], data[0]);
                    magazine.numberOfAvailable = Integer.valueOf(data[2]);
                    magazine.numberOfAll = Integer.valueOf(data[2]);
                    addItemToLibrary(magazine);
                }
            }
            csvReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void exportUsersWithItemsToFile(String csvFile) {
        try {
            FileWriter myWriter = new FileWriter("plik_wynikowy.csv");

            for (User user : listOfUsers) {
                if (user.getRentedItems().isEmpty()) continue;
                myWriter.write("ID" + user.getCardNumber() + "[");
                for (Item item : user.getRentedItems()) {
                    if (item instanceof Book) myWriter.write(item.getTitle() + "-" + ((Book) item).getAuthor() + ";");
                    else if (item instanceof Magazine)
                        myWriter.write(item.getTitle() + "-" + ((Magazine) item).getNumber() + ";");
                }
                myWriter.write("]\n");
            }
            myWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Item> getListOfItems() {
        return listOfItems;
    }

    public ArrayList<User> getListOfUsers() {
        return listOfUsers;
    }

}
