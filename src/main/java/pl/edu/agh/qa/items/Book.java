package pl.edu.agh.qa.items;

public class Book extends Item {

    private String author;

    public Book(String author, String title) {
        super(title);
        this.author = author;
    }

    public void print() {
        String str = "";
        str += getTitle() + ";";
        str += getAuthor() + ";";
        str += getNumberOfAll() + ";";
        str += getNumberOfAvailable() + ";";

        System.out.println(str);
    }

    public String getAuthor() {
        return author;
    }


}
