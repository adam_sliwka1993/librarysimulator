package pl.edu.agh.qa.items;

public class Item {
    private String title;
    public Integer numberOfAvailable;
    public Integer numberOfAll;


    public Item(String title) {
        this.title = title;
        this.numberOfAll = 1;
        this.numberOfAvailable = 1;
    }

    public void print() {

    }

    public String getTitle() {
        return title;
    }

    public Integer getNumberOfAvailable() {
        return numberOfAvailable;
    }

    public Integer getNumberOfAll() {
        return numberOfAll;
    }
}
