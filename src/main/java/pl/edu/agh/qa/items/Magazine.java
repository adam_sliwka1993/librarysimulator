package pl.edu.agh.qa.items;

public class Magazine extends Item {
    private String number;

    public Magazine(String number, String title) {
        super(title);
        this.number = number;
    }

    public void print() {
        String str = "";
        str += getTitle() + ";";
        str += getNumber() + ";";
        str += getNumberOfAll() + ";";
        str += getNumberOfAvailable() + ";";

        System.out.println(str);
    }

    public String getNumber() {
        return number;
    }
}
